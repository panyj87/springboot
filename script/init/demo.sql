/*
 *网站首页
 */
DROP TABLE IF EXISTS yl_home_page_news;
CREATE TABLE `yl_home_page_news`
(
    `id`              varchar(64)         NOT NULL,
    `news_title`      varchar(60)         NOT NULL COMMENT '新闻标题',
    `is_headlines`    TINYINT(1)          NOT NULL COMMENT '是否头条',
    `release_time`    datetime                     DEFAULT NULL COMMENT '发布时间',
    `is_release`      TINYINT(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否发布',
    `type`            varchar(20)         NOT NULL COMMENT '类型 ',
    `content`         text COMMENT '内容',
    `is_deleted`      tinyint(1) unsigned NOT NULL COMMENT '删除标记',
    `create_user`     varchar(60)         NOT NULL COMMENT '创建用户',
    `create_time`     datetime            NOT NULL COMMENT '创建时间',
    `update_user`     varchar(60)         NULL COMMENT '更新用户',
    `update_time`     datetime            NULL COMMENT '更新时间',
    `index_title_url` varchar(2000)                DEFAULT '' COMMENT '图片标题路径',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='首页新闻';

ALTER TABLE `yl_home_page_news`
    ADD `release_time` datetime DEFAULT NULL COMMENT '发布时间';
ALTER TABLE `yl_home_page_news`
    ADD `is_release` TINYINT(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否发布';

ALTER TABLE `yl_cartoon_page_news`
    ADD `release_time` datetime DEFAULT NULL COMMENT '发布时间';
ALTER TABLE `yl_cartoon_page_news`
    ADD `is_release` TINYINT(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否发布';
/*
 * 动态信息
 */
DROP TABLE IF EXISTS yl_cartoon_page_news;
CREATE TABLE `yl_cartoon_page_news`
(
    `id`            varchar(64)         NOT NULL,
    `news_title`    varchar(60)         NOT NULL COMMENT '新闻标题',
    `type`          varchar(20)         NOT NULL COMMENT '类型 ',
    `release_time`  datetime                     DEFAULT NULL COMMENT '发布时间',
    `is_release`    TINYINT(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否发布',
    `content`       text COMMENT '内容',
    `is_deleted`    tinyint(1) unsigned NOT NULL COMMENT '删除标记',
    `create_user`   varchar(60)         NOT NULL COMMENT '创建用户',
    `create_time`   datetime            NOT NULL COMMENT '创建时间',
    `update_user`   varchar(60)         NULL COMMENT '更新用户',
    `update_time`   datetime            NULL COMMENT '更新时间',
    `pic_title_url` varchar(2000)                DEFAULT '' COMMENT '图片标题路径',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='动态信息';
/*
 *信息公开新闻部分
 */
DROP TABLE IF EXISTS yl_information_page_news;
CREATE TABLE `yl_information_page_news`
(
    `id`           varchar(64)         NOT NULL,
    `news_title`   varchar(60)         NOT NULL COMMENT '新闻标题',
    `release_time` datetime                     DEFAULT NULL COMMENT '发布时间',
    `is_release`   TINYINT(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否发布',
    `type`         varchar(20)         NOT NULL COMMENT '类型 ',
    `content`      text COMMENT '内容',
    `is_deleted`   tinyint(1) unsigned NOT NULL COMMENT '删除标记',
    `create_user`  varchar(60)         NOT NULL COMMENT '创建用户',
    `create_time`  datetime            NOT NULL COMMENT '创建时间',
    `update_user`  varchar(60)         NULL COMMENT '更新用户',
    `update_time`  datetime            NULL COMMENT '更新时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='信息公开新闻';

/*
   *信息组织机构部分
   */
DROP TABLE IF EXISTS yl_organization_page_news;
CREATE TABLE `yl_organization_page_news`
(
    `id`          varchar(64)         NOT NULL,
    `news_title`  varchar(60)         NOT NULL COMMENT '新闻标题',
    `type`        varchar(20)         NOT NULL COMMENT '类型 ',
    `content`     text COMMENT '内容',
    `is_deleted`  tinyint(1) unsigned NOT NULL COMMENT '删除标记',
    `create_user` varchar(60)         NOT NULL COMMENT '创建用户',
    `create_time` datetime            NOT NULL COMMENT '创建时间',
    `update_user` varchar(60)         NULL COMMENT '更新用户',
    `update_time` datetime            NULL COMMENT '更新时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='信息组织机构';
/*
* 信息公开--领导班子
*/
DROP TABLE IF EXISTS yl_information_leader_page_news;
CREATE TABLE `yl_information_leader_page_news`
(
    `id`               varchar(64)         NOT NULL,
    `leader_name`      varchar(60)         NOT NULL COMMENT '领导姓名',
    `division_work`    varchar(2000)       NOT NULL COMMENT '工作分工 ',
    `resume`           varchar(2000)       NOT NULL COMMENT '简历 ',
    `type`             varchar(20)         NOT NULL COMMENT '类型 ',
    `is_deleted`       tinyint(1) unsigned NOT NULL COMMENT '删除标记',
    `create_user`      varchar(60)         NOT NULL COMMENT '创建用户',
    `create_time`      datetime            NOT NULL COMMENT '创建时间',
    `update_user`      varchar(60)         NULL COMMENT '更新用户',
    `update_time`      datetime            NULL COMMENT '更新时间',
    `leader_title_url` varchar(2000) DEFAULT '' COMMENT '图片标题路径',
    `leading_group_a`  varchar(1000)       NOT NULL COMMENT '领导班子A',
    `leading_group_b`  varchar(1000)       NOT NULL COMMENT '领导班子B',
    `leading_group_c`  varchar(1000) DEFAULT NULL COMMENT '领导班子C',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='领导班子';

/*
* 走进榆林
*/
DROP TABLE IF EXISTS yl_knowyl_page_news;
CREATE TABLE `yl_knowyl_page_news`
(
    `id`                 varchar(64)         NOT NULL,
    `news_title`         varchar(60)         NOT NULL COMMENT '新闻标题',
    `type`               varchar(20)         NOT NULL COMMENT '类型 ',
    `content`            text COMMENT '内容',
    `is_deleted`         tinyint(1) unsigned NOT NULL COMMENT '删除标记',
    `create_user`        varchar(60)         NOT NULL COMMENT '创建用户',
    `create_time`        datetime            NOT NULL COMMENT '创建时间',
    `update_user`        varchar(60)         NULL COMMENT '更新用户',
    `update_time`        datetime            NULL COMMENT '更新时间',
    `know_pic_title_url` varchar(2000) DEFAULT '' COMMENT '图片标题路径',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='走进榆林';

/*
 *专题专栏
 */
DROP TABLE IF EXISTS yl_special_column_page_news;
CREATE TABLE `yl_special_column_page_news`
(
    `id`                    varchar(64)         NOT NULL,
    `special_column_id`     varchar(60)   DEFAULT NULL COMMENT '当类型为专题新闻时 此属性有值',
    `news_title`            varchar(60)         NOT NULL COMMENT '新闻标题',
    `is_headlines`          TINYINT(1)          NOT NULL COMMENT '是否头条',
    `type`                  varchar(20)         NOT NULL COMMENT '类型 ',
    `content`               text COMMENT '内容',
    `is_deleted`            tinyint(1) unsigned NOT NULL COMMENT '删除标记',
    `create_user`           varchar(60)         NOT NULL COMMENT '创建用户',
    `create_time`           datetime            NOT NULL COMMENT '创建时间',
    `update_user`           varchar(60)         NULL COMMENT '更新用户',
    `update_time`           datetime            NULL COMMENT '更新时间',
    `special_pic_title_url` varchar(2000) DEFAULT '' COMMENT '图片标题路径',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='专题专栏';

/*
 * 用户
 */
# DROP TABLE IF EXISTS yl_user;
CREATE TABLE yl_user
(
    id          VARCHAR(64)         NOT NULL,
    username    VARCHAR(60)         NOT NULL COMMENT '用户名',
    password    VARCHAR(60)         NOT NULL COMMENT '密码',
    salt        VARCHAR(60)         NOT NULL COMMENT '加密混淆码',
    type        VARCHAR(20)         NOT NULL COMMENT '类型: 超级管理员,管理员,普通用户',
    status      VARCHAR(20)         NOT NULL COMMENT '禁用,启用',
    real_name   VARCHAR(100) DEFAULT NULL COMMENT '用户实名',
    gender      VARCHAR(20)  DEFAULT NULL COMMENT '性别',
    mobile      VARCHAR(100) DEFAULT NULL COMMENT '手机',
    email       VARCHAR(100) DEFAULT NULL COMMENT '电子邮箱',
    perm        VARCHAR(100) DEFAULT NULL COMMENT '权限',
    is_deleted  TINYINT(1) UNSIGNED NOT NULL COMMENT '删除标记',
    create_user VARCHAR(60)         NOT NULL COMMENT '创建用户',
    create_time DATETIME            NOT NULL COMMENT '创建时间',
    update_user VARCHAR(60)  DEFAULT NULL COMMENT '更新用户',
    update_time DATETIME     DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='用户';
ALTER TABLE yl_user
    ADD unique (`username`);
DELETE
FROM yl_user
WHERE id = 'admin';
-- admin/admin123
INSERT INTO yl_user (id, username, password, salt, type, status, real_name, gender, mobile, email, perm, is_deleted,
                     create_user, create_time, update_user, update_time)
VALUES ('admin', 'admin', '130598115a8c1f97baebd9463c55848ade7beac3', '20d684592181b4f6', '超级管理员', '启用', null, null,
        null, null, '后台模块,用户管理,首页新闻,动态信息,专题专栏,信息新闻,组织机构,领导班子,走进榆林', 0, '1', '2020-01-01 00:00:00', null, null);
 /**
   *   互动留言
  */
DROP TABLE IF EXISTS yl_interactive_page_news;
CREATE TABLE `yl_interactive_page_news`
(
    `id`             varchar(64)         NOT NULL,
    `quiz_column_id` varchar(60) DEFAULT NULL COMMENT '提出问题时 此属性有值',
    `recovery_time`   datetime            NOT NULL COMMENT '回复时间',
    `type`           varchar(20)         NOT NULL COMMENT '类型 ',
    `content`        text COMMENT '内容',
    `is_deleted`     tinyint(1) unsigned NOT NULL COMMENT '删除标记',
    `create_user`    varchar(60)         NOT NULL COMMENT '创建用户',
    `create_time`    datetime            NOT NULL COMMENT '创建时间',
    `update_user`    varchar(60)         NULL COMMENT '更新用户',
    `update_time`    datetime            NULL COMMENT '更新时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='互动留言';
