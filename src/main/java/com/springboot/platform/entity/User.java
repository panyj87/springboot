package com.springboot.platform.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@TableName(value = "user")
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private String username; //'用户名',
    private String password; //'密码',
    private String salt; //'加密混淆码',
    private String type; //'角色类型: 超级管理员,管理员,普通用户',
    private String status; //'禁用,启用',
    private String realName; //'用户实名',
    private String gender; //'性别',
    private String mobile; //'手机',
    private String email; //'电子邮箱',
    private String perm; //'权限',

    private Boolean isDeleted;
    private String createUser;//创建用户
    private Date createTime;//创建时间
    private String updateUser;//更新用户
    private Date updateTime;//更新时间
}