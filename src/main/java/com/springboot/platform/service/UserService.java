package com.springboot.platform.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.springboot.platform.entity.User;
import org.springframework.stereotype.Service;

@Service
public interface UserService extends IService<User> {
}
