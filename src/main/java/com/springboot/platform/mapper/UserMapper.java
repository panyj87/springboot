package com.springboot.platform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.springboot.platform.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author Allen <panyj87@126.com>
 * @Date 2020/2/11 14:25
 * @Description //TODO
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
}
