package com.springboot;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author Allen <panyj87@126.com>
 * @Date 2020/1/12 19:51
 * @Description //TODO
 */

@Controller
public class HelloController {
    @RequestMapping("/index")
    public String index(){
        return "index";
//        return "demo";
    }
    @RequestMapping("/demo")
    public String demo(){
        return "demo";
//        return "demo";
    }
}